#include <iostream>
#include "Factorial.h"
int main(int argc, char* argv[])
{
    if( argc != 2 ) {
        std::cout << "Invaild Input. (example: \"factorial 1\")" << std::endl;
        return -1;
    }
    int n = atoi(argv[1]);
    auto factorial = Factorial::GetFactorial(n);
    std::cout << "The factorial of " << n << " = " << factorial << std::endl;    
    return 0;
}
